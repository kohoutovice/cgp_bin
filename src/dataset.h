#ifndef DATASET_H
#define DATASET_H value

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include "compiler.h"
#include <cstring>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <vector>
#include "cgp.h"

using namespace std;
using namespace cv;

class Dataset {
private:
    vector<vzorek> data;
    vector<long> img_index;
    vector<uchar> filterred;

public:
    Dataset (){};

    void addImage(string filename){
        Mat input = imread(filename, CV_8UC1); // Read the file
        img_index.push_back(data.size());
        for (int i = 1; i < input.rows-1; i++){
            for (int j = 1; j < input.cols-1; j++){
                vzorek v;
                v.input[0] = input.at<uchar>(i - 1, j - 1);
                v.input[1] = input.at<uchar>(i - 1, j);
                v.input[2] = input.at<uchar>(i - 1, j + 1);

                v.input[3] = input.at<uchar>(i, j - 1);
                v.input[4] = input.at<uchar>(i, j);
                v.input[5] = input.at<uchar>(i, j + 1);

                v.input[6] = input.at<uchar>(i + 1, j - 1);
                v.input[7] = input.at<uchar>(i + 1, j);
                v.input[8] = input.at<uchar>(i + 1, j + 1);

                v.label = sobelIt(v);
                data.push_back(v);
            }
        }
    }

    long size(){
        return data.size();
    }
    vzorek at(long i){
        return data.at(i);
    }

    int getImageIndex(int image_id){
        return img_index[image_id];
    }

    int getImageID(long data_index){
        for (size_t i = 0; i < img_index.size(); i++) {
            if (data_index < img_index[i]) {
                return i-1;
            }
        }
        if (data_index < data.size()) {
            return img_index.size() -1;
        }
        return -1;
    }

    int getImageCount(){
        return img_index.size();
    }

    vzorek& operator[] (int x) {
          return data[x];
    }

    uchar sobelIt(vzorek vz){
        int x0 = vz.input[0] * (-1./8.);
        int x1 = vz.input[1] * (-2./8.);
        int x2 = vz.input[2] * (-1./8.);
        int x3 = vz.input[3] * (0./8.);
        int x4 = vz.input[4] * (0./8.);
        int x5 = vz.input[5] * (0./8.);
        int x6 = vz.input[6] * (1./8.);
        int x7 = vz.input[7] * (2./8.);
        int x8 = vz.input[8] * (1./8.);
        int vysl = 128 + x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8;
        return (uchar) vysl;
    }

    void pushFilterred(uchar value){
        filterred.push_back(value);
    }

    void clearFilterred(){
        filterred.clear();
    }

    void reconstructImage(int image_id, string filename){
        if (image_id < img_index.size()) {
            long img_start = img_index[image_id];
            long img_end = (image_id == img_index.size()-1)? data.size():img_index[image_id+1];
            long img_size = img_end-img_start;
            int width = sqrt(img_size);
            int height = sqrt(img_size);
            if (filterred.size() >= img_end) {

                Mat im(width, height, CV_8U, Scalar(127));

                for (size_t i = 0; i < img_size; i++) {
                    im.at<uchar>(i/width,i%width) = filterred[img_start + i];
                }
                imwrite(filename, im);
            }
        }
    }

};

#endif
