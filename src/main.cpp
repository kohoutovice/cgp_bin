#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include "compiler.h"
#include <cstring>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include "native.h"
#include "cgp.h"
#include "dataset.h"
#include <limits>
using namespace cv;
using namespace std;

chromosome_t *population[MAX_POPSIZE] = {};
int population_fitness[MAX_POPSIZE]; //fitness jedincu populace
float fitt[MAX_POPSIZE] = {};

int block_in = 2; //pocet vstupu  jednoho bloku (neni impl pro zmenu)

int collumn_size = PARAM_R*(block_in+1); //pocet polozek ktery zabira sloupec v chromozomu
int outputs_index = PARAM_C*collumn_size; //index v poli chromozomu, kde zacinaji vystupy
int chromozom_size = outputs_index + PARAM_OUT;
int max_index_computational_unit  = PARAM_R*PARAM_C + PARAM_IN; //max. index pouzitelny jako vstup  pro vystupy

float bestfit;
int bestfit_idx;

collumn_valid **col_valid; //valid gene values for each column

nodetype*  nodeoutput; //array of node outputs used for CGP evaluation
int*       isused[MAX_POPSIZE]; //array of marked used nodes for each individual


Dataset dataset;
Dataset validation_dataset;

bool saveImage = false;
Mat filterred(298, 298, CV_8U, Scalar(127));

int used_nodes(chromosome_t p_chrom, int* isused)
{
    int in, fce, idx, used = 0;
    int *pchrom;

    memset(isused, 0, max_index_computational_unit*sizeof(int));

    //mark nodes connected to the primary outputs
    pchrom = p_chrom + outputs_index;
    for (int i=0; i < PARAM_OUT; i++)
        isused[*pchrom++] = 1;

    //go throught the cgp array
    pchrom = p_chrom + outputs_index - 1;
    idx = max_index_computational_unit-1;
    for (int i=PARAM_C; i > 0; i--)
    {
        for (int j=PARAM_R; j > 0; j--,idx--)
        {
            fce = *pchrom--; //fce
            if (isused[idx] == 1)
            {
               // the current node is marked, mark also the connected nodes
               in = *pchrom--; // in2
               if (fce > 1)    // 2-input functions
                  isused[in] = 1;
               in = *pchrom--; // in1
               isused[in] = 1;

               used++;
            } else {
               // the current node is not market, skip it
               pchrom -= 2;
            }
        }
    }

    return used;
}


inline void evaluation(int parentidx = -1) {

    chromosome_t p_chrom;

    ///generate native code for each candidate solution
    for (int i=0; i < MAX_POPSIZE; i++)
    {
        if (i == parentidx && !saveImage) continue;

        cgp_compile(code[i], (chromosome_t) population[i], isused[i]);
        fitt[i] = 0;
    }

    for (int l=0; l < dataset.size(); l++){
        ///copy the first part of a training vector to the primary inputs
        memcpy(nodeoutput, dataset.at(l).input, PARAM_IN*sizeof(nodetype));
        ///determine and check response of each candidate solution
        for (int i=0; i < MAX_POPSIZE; i++){
            if (i == parentidx && !saveImage ){
                continue;
            }

            /// cgp_eval((chromosome) population[i], isused[i]);
            ((evalfunc *)(code[i]))();

            ///compare the output values against training vector (specification)
            p_chrom = (chromosome_t) population[i] + outputs_index;
            uchar vysl = nodeoutput[*p_chrom];
            fitt[i] += abs(vysl-dataset[l].label);
            if (i == parentidx && dataset.getImageID(l) == 0) {
                filterred.at<uchar>((int)l/(IMAGEWIDTH-2),l%(IMAGEWIDTH-2)) = vysl;
            }
        }
    }
    for (int i=0; i < MAX_POPSIZE; i++){
        if (i == parentidx && !saveImage){
            continue;
        }
        fitt[i] = fitt[i]/((double)((IMAGEWIDTH-2)*(IMAGEHEIGH-2)));
    }
    if (saveImage) {
        static int c = 0;
        imwrite("results/filtered"+ to_string(c++) +".png",filterred);
        imwrite("best.png",filterred);
        saveImage = false;
    }
}

float validate(int individual) {
    validation_dataset.clearFilterred();
    chromosome_t p_chrom;
    float validation_fitness = 0;
    ///generate native code for each candidate solution
    cgp_compile(code[individual], (chromosome_t) population[individual], isused[individual]);

    for (int l=0; l < validation_dataset.size(); l++){
        memcpy(nodeoutput, validation_dataset[l].input, PARAM_IN*sizeof(nodetype));
        ((evalfunc *)(code[individual]))();

        ///compare the output values against training vector (specification)
        p_chrom = (chromosome_t) population[individual] + outputs_index;
        uchar vysl = nodeoutput[*p_chrom];
        validation_fitness += abs(vysl-validation_dataset[l].label);
        validation_dataset.pushFilterred(vysl);
    }
    validation_fitness = validation_fitness/((double)((IMAGEWIDTH-2)*(IMAGEHEIGH-2)));
    static int c = 0;
    validation_dataset.reconstructImage(0, "results/validated"+ to_string(c++) +".png");
    validation_dataset.reconstructImage(0, "best_validated.png");
    return validation_fitness;
}


void mutate(chromosome_t chrom) {
    int mutation_count = (rand()%MUTATION_LIMIT);
    for (int j = 0; j <= mutation_count; j++) {
        int i = rand() % chromozom_size;
        int collumn = (int) (i / collumn_size);
        if (i < outputs_index) {
            if ((i%3) < 2) { //input mutation
                chrom[i] = col_valid[collumn]->values[(rand() % (col_valid[collumn]->count))];
            } else { //mutace fce
               chrom[i] = rand() % FUNCTIONS;
            }
        } else { //mutace vystupu
           chrom[i] = rand() % max_index_computational_unit;
        }
    }
}


void init_data(){
    dataset.addImage("../dataset/bw/airplane.png");
    dataset.addImage("../dataset/bw/arctichare.png");
    dataset.addImage("../dataset/bw/baboon.png");
    dataset.addImage("../dataset/bw/boat.png");
    dataset.addImage("../dataset/bw/lena.png");
    validation_dataset.addImage("../dataset/bw/cat.png");
    validation_dataset.addImage("../dataset/bw/mountain.png");
    validation_dataset.addImage("../dataset/bw/zelda.png");

}


int used_blocks(chromosome_t p_chrom){
    return 1000;
}

int main(int argc, char const *argv[]) {

    init_data();

    srand((unsigned) time(NULL)); //inicializace pseudonahodneho generatoru

    for (int i=0; i < MAX_POPSIZE; i++){ //alokace pameti pro chromozomy populace
        population[i] = new chromosome_t [outputs_index + PARAM_OUT];
        isused[i] = new int [PARAM_C*PARAM_R+PARAM_IN+PARAM_OUT];
        code[i] = malloc_aligned(PARAM_C*PARAM_R*MAXCODESIZE + 64);
    }
    nodeoutput = new nodetype [PARAM_C*PARAM_R+PARAM_IN+PARAM_OUT];
    memset(nodeoutput, 0, sizeof(nodetype) * (PARAM_C*PARAM_R+PARAM_IN+PARAM_OUT));
    //-----------------------------------------------------------------------
    //Priprava pole moznych hodnot vstupu pro sloupec podle l-back a ostatnich parametru
    //-----------------------------------------------------------------------
    col_valid = new collumn_valid *[PARAM_C];
    for (int i=0; i < PARAM_C; i++) {
        col_valid[i] = new collumn_valid;

        int minidx = PARAM_R*(i-L_BACK) + PARAM_IN;
        if (minidx < PARAM_IN) minidx = PARAM_IN; //vystupy bloku zacinaji od PARAM_IN do PARAM_IN+m*n
        int maxidx = i*PARAM_R + PARAM_IN;

        col_valid[i]->count = PARAM_IN + maxidx - minidx;
        col_valid[i]->values = new int [col_valid[i]->count];

        int j=0;
        for (int k=0; k < PARAM_IN; k++,j++) //vlozeni indexu vstupu komb. obvodu
            col_valid[i]->values[j] = k;
        for (int k=minidx; k < maxidx; k++,j++) //vlozeni indexu moznych vstupu ze sousednich bloku vlevo
            col_valid[i]->values[j] = k;
    }

    //-----------------------------------------------------------------------
    //Vytvoreni pocatecni populace
    //-----------------------------------------------------------------------
    chromosome_t p_chrom;
    int collumn;
    for (int i=0; i < MAX_POPSIZE; i++) {
        p_chrom = (chromosome_t) population[i];
        for (int j=0; j < PARAM_R*PARAM_C; j++) {
            collumn = (int)(j / PARAM_R);
            // vstup 1
            *p_chrom++ = col_valid[collumn]->values[(rand() % (col_valid[collumn]->count))];
            // vstup 2
            *p_chrom++ = col_valid[collumn]->values[(rand() % (col_valid[collumn]->count))];
            // funkce
            *p_chrom++ = rand() % FUNCTIONS;
        }
        for (int j=outputs_index; j < outputs_index+PARAM_OUT; j++)  //napojeni vystupu
            *p_chrom++ = rand() % max_index_computational_unit;
    }


    //-----------------------------------------------------------------------
    //Ohodnoceni pocatecni populace
    //-----------------------------------------------------------------------
    bestfit = INT_MAX; bestfit_idx = -1;
    evaluation();
    for (int i=0; i < MAX_POPSIZE; i++) { //nalezeni nejlepsiho jedince
        if (fitt[i] < bestfit) {
           bestfit = fitt[i];
           bestfit_idx = i;
        }
    }

    //bestfit_idx ukazuje na nejlepsi reseni v ramci populace
    //bestfit obsahuje fitness hodnotu prvku s indexem bestfit_idx

    if (bestfit_idx == -1)
       return 1;


    //-----------------------------------------------------------------------
    // EVOLUCE
    //-----------------------------------------------------------------------
    std::cout << "Starting evolution. Parameters:" << '\n';
    for (int generation = 0; generation < MAX_GENERATIONS; generation++) {
        //-----------------------------------------------------------------------
        //mutace nejlepsiho jedince populace (na MAX_POPSIZE mutantu)
        //-----------------------------------------------------------------------
        for (int i=0, midx = 0; i < MAX_POPSIZE;  i++, midx++) {
            if (bestfit_idx == i) continue;
            chromosome_t pc = (int *) copy_chromozome(population[bestfit_idx],population[midx]);
            mutate(pc);
        }

        //-----------------------------------------------------------------------
        //ohodnoceni populace
        //-----------------------------------------------------------------------
        evaluation(bestfit_idx);

        for (int i=0; i < MAX_POPSIZE; i++) {
            if (i == bestfit_idx) continue; //preskocime rodice

            if (fitt[i] <= bestfit) {
                if(fitt[i] < bestfit){
                    float validation_loss = validate(bestfit_idx);
                    cout << "Generation: " << generation << "\t" << "Loss: " << fitt[i]  << " Validation loss: " << validation_loss<< '\n';

                    saveImage = true;
                }
                if (generation%1000 == 0) {
                    cout << "Generation: " << generation << "\t" << "Loss: " << fitt[i] << '\n';
                }

               bestfit_idx = i;
               bestfit = fitt[i];
            }
        }
    }

    return 0;
}
