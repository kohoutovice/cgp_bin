#ifndef CGP_H
#define CGP_H value

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include <cstring>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

#define IMAGEWIDTH 300
#define IMAGEHEIGH 300

#define MAX_GENERATIONS 50000
#define MAX_POPSIZE 8
#define MUTATION_LIMIT 50

#define PARAM_C 15 //pocet sloupcu
#define PARAM_R 15 //pocet radku
#define PARAM_IN 9 //pocet vstupu komb. obvodu
#define PARAM_OUT 1 //pocet vystupu komb. obvodu
#define L_BACK 1

#define FUNCTIONS 14 //max. pocet pouzitych funkci bloku (viz fitness() )


typedef int* chromosome_t;

typedef struct { //struktura obsahujici mozne hodnoty vstupnich poli chromozomu pro urcity sloupec
    int count;   //pouziva se pri generovani noveho cisla pri mutaci
    int *values;
} collumn_valid;

typedef int nodetype;
typedef int fitvaltype;

#define copy_chromozome(from,to) (chromosome_t *) memcpy(to, from, (outputs_index + PARAM_OUT)*sizeof(int));

typedef struct { nodetype input[9]; nodetype label; } vzorek;

extern nodetype*  nodeoutput; //array of node outputs used for CGP evaluation




/*
void init_data(){
    Mat input, output;
    input = imread("../dataset/bw/lena.png", CV_8UC1); // Read the file
    output = imread("../dataset/sobel/lena.png", CV_8UC1); // Read the file
    int counter = 0;
    for (int i = 1; i < input.rows-1; i++){
        for (int j = 1; j < input.cols-1; j++){
            dataset[counter].input[0] = ((i > 0) && (j>0)) ? input.at<uchar>(i - 1, j - 1) : 0;
            dataset[counter].input[1] = ((i > 0) ) ? input.at<uchar>(i - 1, j) : 0;
            dataset[counter].input[2] = ((i > 0) && (j<input.cols-1)) ? input.at<uchar>(i - 1, j + 1) : 0;

            dataset[counter].input[3] = ((j>0)) ? input.at<uchar>(i, j - 1) : 0;
            dataset[counter].input[4] = input.at<uchar>(i, j);
            dataset[counter].input[5] = (j<input.cols-1) ? input.at<uchar>(i, j + 1) : 0;

            dataset[counter].input[6] = ((i < input.rows-1) && (j>0)) ? input.at<uchar>(i + 1, j - 1) : 0;
            dataset[counter].input[7] = ((i < input.rows-1)) ? input.at<uchar>(i + 1, j) : 0;
            dataset[counter].input[8] = ((i < input.rows-1) && (j<input.cols-1)) ? input.at<uchar>(i + 1, j + 1) : 0;

            dataset[counter].label = input.at<uchar>(i, j);
            counter++;
        }
    }
}
*/

#endif
